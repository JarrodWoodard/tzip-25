```
title: Frivolous Proposal Mitigation
status: Draft
author: Jarrod Woodard jarrodwoodard@protonmail.com
type: LA
created: 2021-12-09
date: 2021-12-09
version: 0.0.1
```

# Abstract

As it stands there is no cost other than ones time to submit a proposal. When this occurs the valuable attention of many is drawn to the event. This is an easily exploitable avenue for bad actors to self promote or otherwise waste the time and attention of the community. This proposal seeks to mitigate this by at least requiring those that submit a proposal to risk some wealth should they waste the time of others through malice, neglegence or incompetence.  

This would be achieved by requiring a bond to be included along with the proposal. Should the proposal not reach a minimal, pre-defined percentage of votes, the bond would be forfeit. The bond should be burned for the time being. In the future it could perhaps be added to an on-chain treasury.

To avoid risking punishing good actors, if the proposal gets a certain amount of attention reflected by percentage of total votes, the bond will be returned. Should the proposal not get enough particiaption to reach this threshold, the fallback method of determining if the bond should be forfiet or not would be up to a simple majority. 

People will not want to see good actors punished and if it looks like they will be, the community will act to make sure their proposal either has enough participation, or a simple majority of 'yay.' 

## Specifications

| Parameter | value |
| ------ | ------ |
| Bond amount | 100 tez |
| Participation threshold | %5 of the possible votes |
| If participation vote threshold met | Bond returned to proposer at end of proposal period |
| If participation vote not met | simple majority |
| If simple majority 'yay' | Bond is returned |
| If simple majority 'nay' | Bond is burned |
| If simple majority 'tie' | Bond is returned | 

## Copyright

Copyright and related rights waived via  [CC0](https://creativecommons.org/publicdomain/zero/1.0/)
